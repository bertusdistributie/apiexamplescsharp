# Welcome
This reporistory holds a C# solution with a project that demonstrates to developers how they can use the Bertus public api to [My.Bertus.com](https://my.bertus.com), our portal for customers.

To use this API you must be a customer and obtain a subscription. We have documentation that can help you achieve this. Please contact our [helpdesk](mailto:helpdesk@bertus.com) about this. 

We also have a portal, especially for API developers. You can use this [portal](https://myapi-portal.bertus.com) to obtain a subscripton and to learn more about our api.

---

## ApiCallerConsoleApp

This .net core console application that demonstrates:

1. How to get an authentication token without user interaction.
2. How to create a RestClient and configure it to talk to our api.
3. How to post a filter to our search API
4. How to get results in multiple batches

*please note that this code is not production ready, instead we tried to focus on keeping the examples simple!*

