﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using RestSharp;

namespace ApiCallerConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine($"USAGE: dotnet {System.Reflection.Assembly.GetExecutingAssembly().GetName().Name} [userId] [password]");
                Console.WriteLine("Please pass user id and password as arguments.");
                Console.ReadKey();
                return;
            }
            var config = new ConfigurationBuilder()
                .AddJsonFile("appSettings.json")
                .AddJsonFile("appSettings.Development.json", true)
                .Build();

            var apiConfig = config.GetSection("Api");

            try
            {
                // get auth token
                var token = GetToken(config["Login:BaseUrl"], args[0], args[1]);

                // create rest client for api
                var client = CreateApiClient(apiConfig["BaseUrl"], 
                    token, apiConfig["ApiSubscriptionKey"]);

                var location = StartSearch(client, apiConfig["AccountNumber"]);

                while (location != null)
                {
                    Console.WriteLine(location);

                    // get data from location
                    var data = GetBatchOfSearchResults(client, location);

                    // present results
                    DisplayResults(data);

                    // get location of next batch
                    location = GetLocationOfNextResults(data);
                }
            }
            catch (HttpRequestException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Done");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// extract next link from search result
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static string GetLocationOfNextResults(dynamic data)
        {
            string location;
            try
            {
                location = data?["Links"]?["next"]?["Href"]?.ToString();
            }
            catch (Exception)
            {
                location = null;
            }

            return location;
        }

        /// <summary>
        /// write some article date to console
        /// </summary>
        /// <param name="collection"></param>
        private static void DisplayResults(dynamic data)
        {
            if (data == null) return;

            var collection = data["Collection"];

            if (collection == null) return;

            foreach (var item in collection)
            {
                Console.WriteLine($"{item["Id"]} {item["Artist"]} {item["Title"]} {item["MediaId"]}");
            }
        }

        /// <summary>
        /// Call API client to get a batch of search results
        /// </summary>
        /// <param name="client">api rest client</param>
        /// <param name="location">location of batch</param>
        /// <returns></returns>
        private static dynamic GetBatchOfSearchResults(IRestClient client, string location)
        {
            var request = new RestRequest(location);

            var response = client.Get(request);

            if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                Console.WriteLine(response.StatusDescription);

                // retry
                Console.WriteLine("Press a key to retry");
                Console.ReadKey();

                response = client.Get(request);
            }

            var data = client.Deserialize<dynamic>(response).Data;

            // your request may be throttled. 
            if ((int) response.StatusCode == 429)
            {
                Console.WriteLine(response.StatusDescription);
                Console.WriteLine(data?["message"]);

                // retry
                Console.WriteLine("Press a key to retry");
                Console.ReadKey();

                return data;
            }

            Console.WriteLine(
                $"{response.StatusCode} : read {((JsonArray) data?["Collection"])?.Count} items.");
            return data;
        }

        /// <summary>
        /// initialize search by posting filter to search api
        /// </summary>
        /// <param name="client"></param>
        /// <param name="accountNumber"></param>
        /// <returns>location where to get the first search results</returns>
        private static string StartSearch(IRestClient client, string accountNumber)
        {
            var request = new RestRequest($"/api/v1/accounts/{accountNumber}/searches");

            request.AddJsonBody(new
            {
                Artist = "ABBA"
            });

            var response = client.Post(request);

            if (!response.IsSuccessful)
            {
                throw new HttpRequestException($"{nameof(StartSearch)}: got {response.StatusCode}: {response.StatusDescription} result\n{response.Content}");
            }

            var location = $"{response.Headers.First(h => h.Name == "Location").Value}&take=50";
            return location;
        }

        /// <summary>
        /// Create a rest client for calling api, adding an authentication header and a api subscription key to every request
        /// </summary>
        /// <param name="apiBaseUrl"></param>
        /// <param name="token"></param>
        /// <param name="apiSubscriptionKey"></param>
        /// <returns></returns>
        private static IRestClient CreateApiClient(string apiBaseUrl, string token, string apiSubscriptionKey)
        {
            var client = new RestClient(apiBaseUrl);

            client.AddDefaultHeader("Authorization",
                $"Bearer {token}");

            client.AddDefaultHeader("Ocp-Apim-Subscription-Key", apiSubscriptionKey);

            return client;
        }

        /// <summary>
        /// Acquire auth token for accessing api
        /// </summary>
        /// <param name="loginSiteBaseUri"></param>
        /// <param name="userName"></param>
        /// <param name="passWord"></param>
        /// <returns></returns>
        private static string GetToken(string loginSiteBaseUri, string userName, string passWord)
        {
            var client = new RestClient(loginSiteBaseUri);

            var request = new RestRequest("/api/token");

            request.AddJsonBody(new
            {
                UserName = userName,
                Password = passWord
            });

            request.AddHeader("Content-Type", "application/json");

            var response = client.Post(request);

            if (!response.IsSuccessful)
            {
                    throw new HttpRequestException($"{nameof(GetToken)}: got {response.StatusCode}: {response.StatusDescription} result\n{response.Content}");
            }

            return client.Deserialize<string>(response).Data;
        }
    }
}
